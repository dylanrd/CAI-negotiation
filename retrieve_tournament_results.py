import json
import pandas as pd


f = open('results/results_summaries.json')
data = json.load(f)
f.close()


print(data[0])

for i in range(len(data)):
    data[i] = data[i].values()

data = pd.DataFrame(data)
data.columns = ["num_offers", "agent_1", "utility_1", "agent_2", "utility_2", "nash_product", "social_welfare", "result"]


sums_1 = data.groupby(["agent_1"])["utility_1"].sum()
sums_2 = data.groupby(["agent_2"])["utility_2"].sum()
sum_utility = sums_1 + sums_2

print(sum_utility)




