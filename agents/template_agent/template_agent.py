import logging
from random import randint
from typing import cast

from geniusweb.actions.Accept import Accept
from geniusweb.actions.Action import Action
from geniusweb.actions.Offer import Offer
from geniusweb.actions.PartyId import PartyId
from geniusweb.bidspace.AllBidsList import AllBidsList
from geniusweb.inform.ActionDone import ActionDone
from geniusweb.inform.Finished import Finished
from geniusweb.inform.Inform import Inform
from geniusweb.bidspace.Interval import Interval
from geniusweb.inform.Settings import Settings
from geniusweb.inform.YourTurn import YourTurn
from geniusweb.issuevalue.Bid import Bid
from geniusweb.issuevalue.Domain import Domain
from geniusweb.issuevalue.Value import Value
from geniusweb.issuevalue.ValueSet import ValueSet
from geniusweb.party.Capabilities import Capabilities
from geniusweb.party.DefaultParty import DefaultParty
from geniusweb.profile.utilityspace.UtilitySpace import UtilitySpace
from geniusweb.profileconnection.ProfileConnectionFactory import (
    ProfileConnectionFactory,
)
from geniusweb.progress.ProgressRounds import ProgressRounds
from geniusweb.utils import val
from geniusweb.profileconnection.ProfileInterface import ProfileInterface
from geniusweb.profile.utilityspace.LinearAdditive import LinearAdditive
from geniusweb.progress.Progress import Progress
from tudelft.utilities.immutablelist.ImmutableList import ImmutableList
from time import sleep, time as clock
from decimal import Decimal
from decimal import Context
import sys
from tudelft_utilities_logging.Reporter import Reporter
from geniusweb.progress.ProgressRounds import ProgressRounds
from geniusweb.bidspace.BidsWithUtility import BidsWithUtility
from time import sleep, time as clock
from agents.template_agent.extended_util_space_group_42 import ExtendedUtilSpace
from agents.template_agent.frequency_opponent_model_group_42 import FrequencyOpponentModel


class TemplateAgent(DefaultParty):
    """
    Template agent that offers random bids until a bid with sufficient utility is offered.
    """

    def __init__(self):
        super().__init__()
        self.getReporter().log(logging.INFO, "party is initialized")
        self._profile = None
        self._me: PartyId = None
        self._last_received_bid: Bid = None
        self._last_received_utility = 0
        self._highest_received_bid: Bid = None
        self._highest_received_utility = 0
        self._estimate_nash = 0
        self._bids_with_util : BidsWithUtility = None
        # self._progress: Progress = None
        self._util_space : LinearAdditive = None
        self._extended_space: ExtendedUtilSpace = None
        self._frequency_opponent_model : FrequencyOpponentModel = None

    def notifyChange(self, info: Inform):
        """This is the entry point of all interaction with your agent after is has been initialised.
        Args:
            info (Inform): Contains either a request for action or information.
        """
        # a Settings message is the first message that will be send to your
        # agent containing all the information about the negotiation session.
        if isinstance(info, Settings):
            self._initialize(info)

        # ActionDone is an action send by an opponent (an offer or an accept)
        elif isinstance(info, ActionDone):
            self._actionDone(info)

        # YourTurn notifies you that it is your turn to act
        elif isinstance(info, YourTurn):
            # execute a turn
            self._myTurn()
            # log that we advanced a turn
            self._progress = self._progress.advance()

        # Finished will be send if the negotiation has ended (through agreement or deadline)
        elif isinstance(info, Finished):
            # terminate the agent MUST BE CALLED
            print("OPPONENT MODEL ", self._frequency_opponent_model.toString())
            self.terminate()
        else:
            self.getReporter().log(
                logging.WARNING, "Ignoring unknown info " + str(info)
            )

    # lets the geniusweb system know what settings this agent can handle
    # leave it as it is for this course
    def getCapabilities(self) -> Capabilities:
        return Capabilities(
            set(["SAOP"]),
            set(["geniusweb.profile.utilityspace.LinearAdditive"]),
        )

    # terminates the agent and its connections
    # leave it as it is for this course
    def terminate(self):
        self.getReporter().log(logging.INFO, "party is terminating:")
        super().terminate()
        if self._profile is not None:
            self._profile.close()
            self._profile = None

    #######################################################################################
    ########## THE METHODS BELOW THIS COMMENT ARE OF MAIN INTEREST TO THE COURSE ##########
    #######################################################################################



    def _initialize(self, info: Settings):
        self._settings: Settings = cast(Settings, info)
        self._me = self._settings.getID()

        # progress towards the deadline has to be tracked manually through the use of the Progress object
        self._progress: ProgressRounds = self._settings.getProgress()

        # the profile contains the preferences of the agent over the domain
        self._profile = ProfileConnectionFactory.create(
            info.getProfile().getURI(), self.getReporter()
        )

        self._bids_with_util = BidsWithUtility.create(cast(LinearAdditive, self._profile.getProfile()))
        self._frequency_opponent_model = FrequencyOpponentModel.create().With(self._profile.getProfile().getDomain(),
                                                                              None)


    def _actionDone(self, actionDone):
        action: Action = cast(ActionDone, actionDone).getAction()
        # if it is an offer, set the last received bid
        if isinstance(action, Offer):
            current_bid = cast(Offer, action).getBid()
            self._last_received_bid = current_bid

            if not action.getActor() == self._me:
                self._updateOpponentModel(action)

            if self._profile.getProfile().getUtility(cast(Offer, action).getBid()) > self._highest_received_utility:
                self._highest_received_utility = self._profile.getProfile().getUtility(cast(Offer, action).getBid())

            self._last_received_bid = cast(Offer, action).getBid()
        pass


    # give a description of your agent
    def getDescription(self) -> str:
        return "Template agent for Collaborative AI course"

    # execute a turn
    def _myTurn(self):
        self._updateUtilSpace()
        print("this is round", self._progress.getCurrentRound())
        print("this is highest so far", self._highest_received_utility)
        print("hoy")
        # check if the last received offer if the opponent is good enough
        if self._isGood(self._last_received_bid):
            # if so, accept the offer
            action = Accept(self._me, self._last_received_bid)
        else:
            # if not, find a bid to propose as counter offer
            bid = self._findBid()
            action = Offer(self._me, bid)

        # send the action
        self.getConnection().send(action)

    def _updateUtilSpace(self) -> LinearAdditive:  # throws IOException
        newutilspace = self._profile.getProfile()
        if not newutilspace == self._util_space:
            self._util_space = cast(LinearAdditive, newutilspace)
            self._extended_space = ExtendedUtilSpace(self._util_space)
        return self._util_space

    # method that checks if we would agree with an offer
    def _isGood(self, bid: Bid) -> bool:
        if bid is None:
            return False
        profile = self._profile.getProfile()

        progress = self._progress.get(0)

        # very basic approach that accepts if the offer is valued above 0.6 and
        # 80% of the rounds towards the deadline have passed
        return profile.getUtility(bid) > self._highest_received_utility and progress > 0.9

    def findUtility(self, bid):
        return self._profile.getProfile().getUtility(bid)

    def _updateOpponentModel(self, offer: Action):
        self._frequency_opponent_model = FrequencyOpponentModel.WithAction(self._frequency_opponent_model, offer, self._progress)
        self._last_received_utility = self.findUtility(self._last_received_bid)
        area = Decimal(Context.multiply(Context(),
                                        self._frequency_opponent_model.getUtility(
                                            self._last_received_bid),
                                        self._last_received_utility))

        if area > self._estimate_nash:
            self._estimate_nash = area
            self._my_nash_utility = self._last_received_utility

    def _findBid(self) -> Bid:
        # compose a list of all possible bids
        domain = self._profile.getProfile().getDomain()
        all_bids = AllBidsList(domain)

        total_range = self._bids_with_util.getRange()
        range_min = total_range.getMin()
        range_max = total_range.getMax()

        # get the bid that are within 20% of the set range
        twenty_percentile = Context.subtract(Context(), range_max, Context.multiply(Context(), Context.subtract(Context(), range_max, range_min), Decimal.from_float(0.95)))
        range_of_bids = self._bids_with_util.getBids(Interval(twenty_percentile, range_max))





        # max_bids = self._extended_space.getBids(self._extended_space.getMax())
        # print("this is max_bids", max_bids)
        # print("we're proposing this bid, ", max_bids.get(0), "with value", self.findUtility(max_bids.get(0)))
        # return max_bids.get(0)
        # contrary_util = 0
        # bid_chosen = None
        # for bid in range_of_bids:
        #     if self._frequency_opponent_model.getUtility(bid) > contrary_util:
        #         contrary_util = self._frequency_opponent_model.getUtility(bid)
        #         bid_chosen = bid

        bid_chosen = range_of_bids.get(randint(0, range_of_bids.size() - 1))


        print("THIS IS OUR ESTIMATION OF OPPONENT UTILITY", self._frequency_opponent_model.getUtility(bid_chosen))
        return bid_chosen



